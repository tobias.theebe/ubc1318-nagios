# Ubee UBC1318 Plugin for Nagios

## Description

This PHP script serves as a simple monitoring solution for the UBC1318 cable modem/router produced by Ubee Interactive. The script retrieves downstream and upstream channel data and presents this to Nagios as HTML + performance data.

While current status information is displayed in table views, performance data is available for further processing into RRD files, allowing time-series data to be visualized in graphing applications such as nagiosgraph and PNP4Nagios. This allows the user to observe the modem's behavior over an extended period of time, which is normally reserved to the ISP.

The script has been written for and tested with a UBC1318ZG provided by Ziggo, a major ISP in the Netherlands. It MIGHT work with UBC1318 modems supplied by other providers and/or other Ubee DOCSIS 3.1 modems.

## Software Requirements

The script has been tested in the following environments:

- Debian 10.9-10.13, 12.2-12.9
- NGINX 1.14.2, 1.22.1
- PHP 7.3.27-7.3.31, 8.2.7-8.2.26 with cURL and JSON modules
- Nagios Core 4.4.6-4.5.9
- NagiosQL 3.4.1, 3.5.0
- nagiosgraph 1.5.2

## Installation

1. Within the Nagios CGI configuration file (`NAGIOS_ROOT/etc/cgi.cfg`), set `escape_html_tags` to `0`.
2. Download `check_ubc1318.php` and move it to the Nagios plugin directory (`NAGIOS_ROOT/libexec/`).
3. Create a new command. Supply the following command line: `php $USER1$/check_ubc1318.php $HOSTADDRESS$`.
4. Create a new host. Use `check_ping` as its check command.
5. Create a new service. Use the newly created command as its check command.
6. Assign the service to the host.
7. Save and verify the new configuration and restart the Nagios daemon.
8. (**Optional but recommended**) Append `common.css` to `NAGIOS_ROOT/share/stylesheets/common.css`.
9. (**Optional**) Append `labels.conf` to `NAGIOSGRAPH_ROOT/etc/labels.conf`.

## Notes

- Using a configuration management application such as NagiosQL is highly recommended.
- In case of RCS/TCS partial service (also known as DS/US partial bonding, modem fails to lock onto one or more assigned DS/US channels), the script will exit with a CRITICAL/WARNING state, depending on the amount of unlocked channels. The script is configured for a 32×5 channel profile (DS SC-QAM ×31, DS OFDM ×1, US ATDMA ×4, US OFDMA ×1) by default. When a different channel profile is in use (e.g. 33×5), the `$rcs_th` and `$tcs_th` variable values on line 31 should be adjusted accordingly to prevent false-positive alerts/notifications.
- In order to generate graphs from acquired channel data, Nagios must be set up to process performance data and an external graphing application (which usually writes/reads to/from RRD files) is required. These topics are beyond the scope of this readme.
