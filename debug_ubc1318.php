<?php
error_reporting(E_ALL ^ E_WARNING);

$host = $argv[1];
$ua =   'debug_ubc1318.php';
$url =  'http://' . $host . '/htdocs/cm_info_connection.php';

dumpData();

function dumpData() {
	global $url, $ua;

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_USERAGENT, $ua);
	$cm_info_connection = curl_exec($ch);
	curl_close($ch);

	preg_match('/cm_conn_json = \'(.*)\'/', $cm_info_connection, $cm_conn_json);
	
	if (!$cm_conn_json[1]) {echo "Error: unable to fetch data.\n";}
	else {print_r(json_decode($cm_conn_json[1])); echo "\n";}
}
?>
