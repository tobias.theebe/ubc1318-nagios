<?php
error_reporting(E_ALL ^ E_WARNING);

define('STATE_OK',       0);
define('STATE_WARNING',  1);
define('STATE_CRITICAL', 2);
define('STATE_UNKNOWN',  3);

$host = $argv[1];
$ua =   'check_ubc1318.php';
$url =  'http://' . $host . '/htdocs/cm_info_connection.php';

returnInfo();

function returnInfo() {
	global $url, $ua;

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_USERAGENT, $ua);
	$cm_info_connection = curl_exec($ch);
	curl_close($ch);

	preg_match('/cm_conn_json = \'(.*)\'/', $cm_info_connection, $cm_conn_json);

	$cm_conn = json_decode($cm_conn_json[1]);

	if (!$cm_conn->cm_conn_ds_gourpObj || !$cm_conn->cm_conn_us_gourpObj) {echo 'DS/US UNKNOWN - No data available'; exit(STATE_UNKNOWN);}

	$rcs_th = 32; $tcs_th = 5;
	$rcs = count($cm_conn->cm_conn_ds_gourpObj); $tcs = count($cm_conn->cm_conn_us_gourpObj);
	$rcs_scqam = 0; $rcs_ofdm = 0; $tcs_atdma = 0; $tcs_ofdma = 0;

	$ds_warn = 0; $ds_crit = 0;
	$ds_status = ''; $ds_ch_status = ''; $ds_ch_mt = '';
	$ds_p = []; $ds_mer = []; $ds_c_scqam = []; $ds_c_ofdm = []; $ds_uc_scqam = []; $ds_uc_ofdm = [];

	$ds_html = '<table class="statusInfo"><tr><th>DCID</th><th><i>f<sub>c</sub></i></th><th>W</th><th>MT/CT</th><th><i>P</i></th><th>MER</th><th>CCE</th><th>UCE</th></tr>';
	$ds_perf = '';

	foreach ($cm_conn->cm_conn_ds_gourpObj as $ds_ch) {
		array_push($ds_p, $ds_ch->ds_power / 10);

		switch ($ds_ch->ds_modulation) {
			case 1: $ds_ch_mt = '?'; break;
			case 2: $ds_ch_mt = 'OFDM'; break;
			case 3: $ds_ch_mt = '64-QAM'; break;
			case 4: $ds_ch_mt = '256-QAM'; break;
			case 5: $ds_ch_mt = '16-QAM'; break;
			case 6: $ds_ch_mt = 'ALL'; break;
			case 7: $ds_ch_mt = '1024-QAM'; break;
			case 8: $ds_ch_mt = '512-QAM'; break;
		}

		if ($ds_ch->ds_modulation == 2) {
			$rcs_ofdm++;

			if ($ds_ch->ds_power < -130 || $ds_ch->ds_power > 170 || $ds_ch->ds_snr < 31) {$ds_crit++; $ds_ch_status = 'statusCRITICAL';}
			elseif ($ds_ch->ds_power < -80 || $ds_ch->ds_power > 110 || $ds_ch->ds_snr < 34) {$ds_warn++; $ds_ch_status = 'statusWARNING';}
			else {$ds_ch_status = 'statusOK';}

			array_push($ds_mer, $ds_ch->ds_snr);
			array_push($ds_c_ofdm, $ds_ch->ds_correct);
			array_push($ds_uc_ofdm, $ds_ch->ds_uncorrect);

			$ds_html .= '<tr><td class="' . $ds_ch_status . '">' . $ds_ch->ds_id . '</td><td>' . number_format($ds_ch->ds_freq / 1000000, 3, '.', '') . '</td><td>' . number_format($ds_ch->ds_width / 1000, 1, '.', '') . '</td><td>' . $ds_ch_mt . '</td><td>' . number_format($ds_ch->ds_power / 10, 1, '.', '') . '</td><td>' . number_format($ds_ch->ds_snr, 1, '.', '') . '</td><td>' . number_format($ds_ch->ds_correct, 0, '', ',') . '</td><td>' . number_format($ds_ch->ds_uncorrect, 0, '', ',') . '</td></tr>';
			// $ds_ch->ds_freq indicates first PLC CP frequency. (PLC = Physical Link Channel, CP = continuous pilot.)
		}
		else {
			$rcs_scqam++;

			if ($ds_ch->ds_power < -130 || $ds_ch->ds_power > 170 || $ds_ch->ds_snr < 310 || $ds_ch->ds_modulation == 1) {$ds_crit++; $ds_ch_status = 'statusCRITICAL';}
			elseif ($ds_ch->ds_power < -80 || $ds_ch->ds_power > 110 || $ds_ch->ds_snr < 340 || $ds_ch->ds_modulation == 3 || $ds_ch->ds_modulation == 5) {$ds_warn++; $ds_ch_status = 'statusWARNING';}
			else {$ds_ch_status = 'statusOK';}

			array_push($ds_mer, $ds_ch->ds_snr / 10);
			array_push($ds_c_scqam, $ds_ch->ds_correct);
			array_push($ds_uc_scqam, $ds_ch->ds_uncorrect);

			$ds_html .= '<tr><td class="' . $ds_ch_status . '">' . $ds_ch->ds_id . '</td><td>' . number_format($ds_ch->ds_freq / 1000000, 3, '.', '') . '</td><td>' . number_format($ds_ch->ds_width / 1000000, 1, '.', '') . '</td><td>' . $ds_ch_mt . '</td><td>' . number_format($ds_ch->ds_power / 10, 1, '.', '') . '</td><td>' . number_format($ds_ch->ds_snr / 10, 1, '.', '') . '</td><td>' . number_format($ds_ch->ds_correct, 0, '', ',') . '</td><td>' . number_format($ds_ch->ds_uncorrect, 0, '', ',') . '</td></tr>';
		}
	}

	if ($ds_crit > 0 || $rcs < $rcs_th) {$ds_status = 'CRITICAL';}
	elseif ($ds_warn > 0) {$ds_status = 'WARNING';}
	else {$ds_status = 'OK';}

	$ds_p_avg = 0; $ds_p_min = 0; $ds_p_max = 0;
	$ds_mer_avg = 0; $ds_mer_min = 0; $ds_mer_max = 0;
	$ds_c_scqam_avg = 0; $ds_uc_scqam_avg = 0;
	$ds_c_ofdm_avg = 0; $ds_uc_ofdm_avg = 0;

	if ($rcs > 0) {
		$ds_p_avg = number_format(array_sum($ds_p) / $rcs, 1, '.', ''); $ds_p_min = number_format(min($ds_p), 1, '.', ','); $ds_p_max = number_format(max($ds_p), 1, '.', ',');
		$ds_mer_avg = number_format(array_sum($ds_mer) / $rcs, 1, '.', ''); $ds_mer_min = number_format(min($ds_mer), 1, '.', ','); $ds_mer_max = number_format(max($ds_mer), 1, '.', ',');

		if ($rcs_scqam > 0) {$ds_c_scqam_avg = number_format(array_sum($ds_c_scqam) / $rcs_scqam, 0, '', ','); $ds_uc_scqam_avg = number_format(array_sum($ds_uc_scqam) / $rcs_scqam, 0, '', ',');}
		if ($rcs_ofdm > 0) {$ds_c_ofdm_avg = number_format(array_sum($ds_c_ofdm) / $rcs_ofdm, 0, '', ','); $ds_uc_ofdm_avg = number_format(array_sum($ds_uc_ofdm) / $rcs_ofdm, 0, '', ',');}
	}
	else {
		$ds_html .= '<tr><td colspan="8" style="text-align: center">(N/A)</td></tr>';
	}

	$ds_html .= '<tr><td style="text-align: center"><i>Avg</i></td><td colspan="2"></td><td><i>Per CT</i></td><td>' . $ds_p_avg . '</td><td>' . $ds_mer_avg . '</td><td>' . $ds_c_scqam_avg . '<br />' . $ds_c_ofdm_avg . '</td><td>' . $ds_uc_scqam_avg . '<br />' . $ds_uc_ofdm_avg . '</td></tr>';
	$ds_html .= '<tr><td style="text-align: center"><i>Min</i></td><td colspan="3"></td><td>' . $ds_p_min . '</td><td>' . $ds_mer_min . '</td><td colspan="2"></td></tr>';
	$ds_html .= '<tr><td style="text-align: center"><i>Max</i></td><td colspan="3"></td><td>' . $ds_p_max . '</td><td>' . $ds_mer_max . '</td><td colspan="2"></td></tr>';
	$ds_html .= '<tr><td></td><td>MHz</td><td>MHz</td><td></td><td>dBmV</td><td>dB</td><td colspan="2"></td></tr></table>';
	$ds_perf .= 'ds-p-avg=' . $ds_p_avg . ' ds-mer-avg=' . $ds_mer_avg . ' ds-c-scqam-avg=' . str_replace(',', '', $ds_c_scqam_avg) . ' ds-c-ofdm-avg=' . str_replace(',', '', $ds_c_ofdm_avg) . ' ds-uc-scqam-avg=' . str_replace(',', '', $ds_uc_scqam_avg) . ' ds-uc-ofdm-avg=' . str_replace(',', '', $ds_uc_ofdm_avg);
	$ds_perf .= ' ds-p-min=' . $ds_p_min . ';-8;-13 ds-p-max=' . $ds_p_max . ';11;17 ds-mer-min=' . $ds_mer_min . ';34;31 ds-mer-max=' . $ds_mer_max;

	$us_warn = 0; $us_crit = 0;
	$us_status = ''; $us_ch_status = '';
	$us_p_atdma = []; $us_p_ofdma = [];

	$us_html = '<table class="statusInfo"><tr><th>UCID</th><th><i>f<sub>c</sub></i></th><th>W</th><th>CT</th><th><i>P</i></th><th>IUC</th></tr>';
	$us_perf = '';

	foreach ($cm_conn->cm_conn_us_gourpObj as $us_ch) {
		if ($us_ch->us_type == 278) {
			$tcs_ofdma++;

			if ($us_ch->us_power < 112 || $us_ch->us_power > 188) {$us_crit++; $us_ch_status = 'statusCRITICAL';}
			elseif ($us_ch->us_power < 120 || $us_ch->us_power > 180) {$us_warn++; $us_ch_status = 'statusWARNING';}
			else {$us_ch_status = 'statusOK';}

			array_push($us_p_ofdma, $us_ch->us_power / 4);

			$us_html .= '<tr><td class="' . $us_ch_status . '">' . $us_ch->us_id . '</td><td>' . number_format($us_ch->us_freq / 1000000, 3, '.', '') . '</td><td>' . number_format($us_ch->us_width / 1000, 1, '.', '') . '</td><td>OFDMA</td><td>' . number_format($us_ch->us_power / 4, 1, '.', '') . '</td><td>' . $us_ch->us_modulation . '</td></tr>';
			// $us_ch->us_freq indicates subcarrier zero (0) frequency.
		}
		else {
			$tcs_atdma++;

			if ($us_ch->us_power < 340 || $us_ch->us_power > 530 || $us_ch->us_modulation == 0) {$us_crit++; $us_ch_status = 'statusCRITICAL';}
			elseif ($us_ch->us_power < 360 || $us_ch->us_power > 510) {$us_warn++; $us_ch_status = 'statusWARNING';}
			else {$us_ch_status = 'statusOK';}

			array_push($us_p_atdma, $us_ch->us_power / 10);

			$us_html .= '<tr><td class="' . $us_ch_status . '">' . $us_ch->us_id . '</td><td>' . number_format($us_ch->us_freq / 1000000, 3, '.', '') . '</td><td>' . number_format($us_ch->us_width / 1000000, 1, '.', '') . '</td><td>ATDMA</td><td>' . number_format($us_ch->us_power / 10, 1, '.', '') . '</td><td></td></tr>';
		}
	}

	if ($us_crit > 0 || $tcs < $tcs_th) {$us_status = 'CRITICAL';}
	elseif ($us_warn > 0) {$us_status = 'WARNING';}
	else {$us_status = 'OK';}

	$us_p_atdma_avg = 0; $us_p_atdma_min = 0; $us_p_atdma_max = 0;
	$us_p_ofdma_avg = 0; $us_p_ofdma_min = 0; $us_p_ofdma_max = 0;

	if ($tcs > 0) {
		if ($tcs_atdma > 0) {$us_p_atdma_avg = number_format(array_sum($us_p_atdma) / $tcs_atdma, 1, '.', ''); $us_p_atdma_min = number_format(min($us_p_atdma), 1, '.', ','); $us_p_atdma_max = number_format(max($us_p_atdma), 1, '.', ',');}
		if ($tcs_ofdma > 0) {$us_p_ofdma_avg = number_format(array_sum($us_p_ofdma) / $tcs_ofdma, 1, '.', ''); $us_p_ofdma_min = number_format(min($us_p_ofdma), 1, '.', ','); $us_p_ofdma_max = number_format(max($us_p_ofdma), 1, '.', ',');}
	}
	else {
		$us_html .= '<tr><td colspan="6" style="text-align: center">(N/A)</td></tr>';
	}

	$us_html .= '<tr><td style="text-align: center"><i>Avg</i></td><td colspan="2"></td><td><i>Per CT</i></td><td>' . $us_p_atdma_avg . '<br />' . $us_p_ofdma_avg . '</td><td></td></tr>';
	$us_html .= '<tr><td style="text-align: center"><i>Min</i></td><td colspan="2"></td><td><i>Per CT</i></td><td>' . $us_p_atdma_min . '<br />' . $us_p_ofdma_min .'</td><td></td></tr>';
	$us_html .= '<tr><td style="text-align: center"><i>Max</i></td><td colspan="2"></td><td><i>Per CT</i></td><td>' . $us_p_atdma_max . '<br />' . $us_p_ofdma_max .'</td><td></td></tr>';
	$us_html .= '<tr><td></td><td>MHz</td><td>MHz</td><td></td><td>dBmV</td><td></td></tr></table>';
	$us_perf .= ' us-p-atdma-avg=' . $us_p_atdma_avg . ' us-p-ofdma-avg=' . $us_p_ofdma_avg . ' us-p-atdma-min=' . $us_p_atdma_min . ';36;34 us-p-atdma-max=' . $us_p_atdma_max . ';51;53 us-p-ofdma-min=' . $us_p_ofdma_min . ';30;28 us-p-ofdma-max=' . $us_p_ofdma_max . ';45;47';

	echo '<table class="statusInfo"><tr><td>' . $ds_html . '</td><td style="vertical-align: top">' . $us_html . '</td></tr></table>';
	echo 'DS ' . $ds_status . ' - DBG (RCS) = ' . $rcs . '/' . $rcs_th  . ', C/W = ' . $ds_crit . '/' . $ds_warn . '<br />';
	echo 'US ' . $us_status . ' - UBG (TCS) = ' . $tcs . '/' . $tcs_th  . ', C/W = ' . $us_crit . '/' . $us_warn;
	echo ' | ' . $ds_perf . $us_perf;

	if ($ds_crit > 0 || $us_crit > 0) {exit(STATE_CRITICAL);}
	elseif ($ds_warn > 0 || $us_warn > 0) {exit(STATE_WARNING);}
	else {exit(STATE_OK);}
}
?>
